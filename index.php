<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Shopping list</title>
	<style>
		body{
			margin: 5vw auto;
			width: 600px;
			max-width: 100%;
			background: linear-gradient(220deg,#8c8f98,#2e3443,#fe3048);
			background-size: 260% 600%;
			background-attachment: fixed;

		}
		form {
			  display: flex;
			  padding: 8px 0;
		}
		input {
			
		}
		a,button{
			padding : 8px 12px;
			color: #fff;
			background: #000;
			text-decoration: none;
			margin: 4px;
			border: none;
			font-size: 14px;
			font-weight: normal;
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<form action="engin.php" method="GET">
		<input type="text" name="name" required>
		<input type="number" name="quantity" min="1" max="100" required>
		<select name="category" required>
			<option value="food">Food</option>
			<option value="snack">Snack</option>
			<option value="other">Other</option>
		</select>
		<input type="hidden" name="action" value="add">
		<input type="hidden" name="shopping_list_ID" value="0">
		<button>add</button>
	</form>

	<section>
		<?php 
		//include 'list.php';
		include 'engin.php';
		$list = Shoping_list::get();
		$data = $list ;
		$data_count = count($data);
			for ($i=0; $i < $data_count; $i++) { 
				?>
					<form class="item" action="engin.php" method="GET">
						<input type="text" name="name" required value="<?php echo $data[$i]['name']?>">
						<input type="number" name="quantity" min="1" max="100" required value="<?php echo $data[$i]['quantity']?>">
						<select name="category" required>

							<option value="food"  autocomplete="off"
								<?php if($data[$i]['category'] == 'food')  echo 'selected '?>>Food</option>
							<option value="snack"<?php if($data[$i]['category'] == 'snack')  echo 'selected '?>>Snack</option>
							<option value="other"<?php if($data[$i]['category'] == 'other')  echo 'selected '?>>Other</option>
						</select>
						<input type="hidden" name="action" value="update">
						<input type="hidden" name="id" value="<?php echo $data[$i]['id']?>">
						<input type="hidden" name="shopping_list_ID" value="<?php echo $data[$i]['shopping_list_ID']?>">
						<button>Update</button>
						<a href="engin.php?action=delete&id=<?php echo $data[$i]['id']?>">Remove</a>
					</form>
				<?php
			}
		?>
	</section>
</body>
</html>