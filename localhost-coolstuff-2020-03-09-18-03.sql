# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Värd: localhost (MySQL 5.7.26)
# Databas: coolstuff
# Genereringstid: 2020-03-09 17:03:23 +0000
# ************************************************************



# Tabelldump shopping_list
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shopping_list`;

CREATE TABLE `shopping_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `shopping_list_ID` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `quantity` int(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shopping_list` WRITE;
/*!40000 ALTER TABLE `shopping_list` DISABLE KEYS */;

INSERT INTO `shopping_list` (`id`, `shopping_list_ID`, `name`, `quantity`, `category`)
VALUES
	(1,0,'Cola',2,'snack'),
	(2,0,'Pizza',1,'food');

/*!40000 ALTER TABLE `shopping_list` ENABLE KEYS */;
UNLOCK TABLES;

