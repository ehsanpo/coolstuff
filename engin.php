<?php 

$whitelist = array(
	'127.0.0.1',
	'::1',
	'localhost'
);

if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
	$dbuser="root";
	$dbpass="root";
	$dbname="coolstuff";
	$dbdsn="mysql:host=localhost". ';dbname=' . $dbname;
}
else{
	$dbuser="";
	$dbpass="";
	$dbname="";
	$dbdsn="mysql:host=localhost". ';dbname=' . $dbname;
}

//get data
$data = $_GET;

//check db connection
try{
	$dbh = new PDO($dbdsn,$dbuser,$dbpass);
}
catch(PDOException $ex){
	err('Unable to connect');
}

//check actions
if(isset($data['action'])){
	$action=$data['action'];
	switch ($action) {
		case "add":
			Shoping_list::add();
			break;
		case "get":
			Shoping_list::get();
			break;
		case "update":
			Shoping_list::update();
			break;
		case "delete":
			Shoping_list::delete();
			break;
		default:
			break;
		}
	header('Location: ' . $_SERVER["HTTP_REFERER"] );
}
else{
	//err('No actions');
}

function err($mes){
	die(json_encode(array('status' => false, 'message' => $mes )));
}
function done($mes = '',$data= []){
	return(array('status' => true, 'message' => $mes, 'data' => $data ));
}

class Shoping_list{

	public static function add(){
		global $dbh,$data;

		try { 
			$stmt = $dbh->prepare('INSERT INTO shopping_list(shopping_list_ID,name, quantity,category) VALUES(:shopping_list_ID,:name,:quantity,:category)');
			$a = $stmt->execute( array(
				'shopping_list_ID'	=> $data['shopping_list_ID'],
				'name' => $data['name'],
				'quantity' => $data['quantity'],
				'category' => $data['category']
			) );
			done('item_added',$a);
			
		}
		catch(PDOException $e) {
			echo $e->getMessage();
			echo "fail";
		}
	}

	public static function get(){
		global $dbh,$data;
		try { 
			$stmt = $dbh->prepare('SELECT * From shopping_list'); 
				//order by shopping_list_ID maybe?
			$stmt->execute();
			$res = $stmt->fetchAll();
			return( $res );
		}
		catch(PDOException $e) {
			echo $e->getMessage();
			echo "fail";
		}
	}

	public static function update(){
		global $dbh,$data;
		if (!isset($data['id']) ) {
			return err('[U03] - Missing id!');
		}
		try { 
			$stmt = $dbh->prepare('UPDATE shopping_list SET shopping_list_ID =:shopping_list_ID, name=:name, quantity=:quantity,category=:category WHERE id = :id');

			$stmt->execute( array(
				'shopping_list_ID'	=> $data['shopping_list_ID'],
				'name' => $data['name'],
				'quantity' => $data['quantity'],
				'category' => $data['category'],
				'id' => $data['id']
				 
			) );
			done('item updated');
		}
		catch(PDOException $e) {
			echo $e->getMessage();
			echo "fail";
		}

	}

	public static function delete(){
		global $dbh,$data;
		if (!isset($data['id'])  ) {
			return err('[U04] - Missing id!');
		}
		try { 
			$stmt = $dbh->prepare('DELETE FROM shopping_list WHERE id = :id');
			$stmt->execute( array(
				'id'	=> $data['id'],
			) );
			done('item deleted');

		}
		catch(PDOException $e) {
			echo $e->getMessage();
			echo "fail";
		}
	}
}
